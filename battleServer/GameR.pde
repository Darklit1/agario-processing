class GameRoom{
  ArrayList<Client> clients = new ArrayList<Client>();
  ArrayList<Player> players = new ArrayList<Player>(2);
  int playerCount = 0;
  int id;
  String ownerIP;
  
  GameRoom(int id, String ip){
    this.id = id;
    ownerIP = ip;
  }
  
  void join(Client cli){
    clients.add(cli);
    playerCount++;
  }
  
  void remove(Client cli){
    for(int i = 0; i < clients.size(); i++){
      if(clients.get(i).ip().equals(cli.ip())) clients.remove(i);
    }
    playerCount--;
  }
  
}