class Player{
  int roomID;
  String ip;
  Ship[] ships;
  Grid playerGrid;
  Grid enemyGrid;
  
  Player(String ip, int room){
    this.ip = ip;
    this.roomID = room;
  }
  
  void processGuess(int row, int col){
    
  }
}

class Grid{
  Location[][] grid = new Location[10][10];
  
  Grid(){
    for(int i = 0; i < 10; i++){
      grid[i] = new Location[10];
    }
  }
  
  boolean isHit(int row, int col){
    return(grid[row][col].getStatus() == 1);
  }
}

class Ship{
  int length;
  int direction;
  int row,col;
  
  Ship(int length){
    this.length = length;
  }
}

class Location{
  boolean ship;
  int row;
  int col;
  int status;
  
  Location(int row, int col){
    this.row = row;
    this.col = col;
    this.status = -1;
  }
  
  boolean hasShip(){
    return ship;
  }
  
  int getStatus(){
    // -1 is Unguessed 0 is missed 1 is hit
    return status;
  }
  
  void setShip(boolean ship){
    this.ship = ship;
  }
  
  void setStatus(int stat){
    status = stat;
  }
  
}