import processing.net.*;
Server server;
ArrayList<Client> connectedClients = new ArrayList<Client>();
ArrayList<GameRoom> rooms = new ArrayList<GameRoom>(10);
void setup(){
  server = new Server(this,5204);
}

void draw(){
  
}

void readData(String data){
  String id = "";
  int startIndex = -1;
  for(int i = 0; i < data.length(); i++){
    
    if(data.charAt(i) == ':'){
      id = data.substring(0,i);
      startIndex = i+1;
      break;
    }
  }
    if(startIndex != -1 && !id.contains(".")){
      String dat = data.substring(startIndex,data.length()-1);
      if(dat.contains(".")){
        for(int g = 0; g < connectedClients.size(); g++){
          if(connectedClients.get(g).ip().equals(dat)){
            for(int q = 0; q < rooms.size(); q++){
              if(rooms.get(q).id == Integer.parseInt(id) && rooms.get(q).playerCount<2){
                sendData(connectedClients.get(g).ip(),"joined");
                break;
              }
            }
            break;
          }
        }
      }
    }else if(startIndex != -1){
      String dat = data.substring(startIndex,data.length()-1);
      if(dat.length() == 4){
        int check = 0;
        for(int i = 0; i < rooms.size(); i++){
          if(rooms.get(i) != null){
            if(Integer.parseInt(dat) == rooms.get(i).id){
              sendData(id,"error");
              break;
             }
          }else check++;
        }
        if(check == rooms.size()){
          sendData(id,"created");
          rooms.add(new GameRoom(Integer.parseInt(dat),id));
        }
      }
    }
  }

void sendData(String id, String data){
  server.write(id + ":" + data);
}

void serverEvent(Server serv, Client cli){
  
}

void disconnectEvent(Client cli){
  for(int i = 0; i < connectedClients.size(); i++){
    if(connectedClients.get(i).ip().equals(cli.ip())){
      connectedClients.remove(i);
    }
  }
}