import processing.net.*;
Client me;
GameRoom[] rooms = new GameRoom[10];
int joining = -1;
boolean joined = false;
Player player;
Player enemy;

int tempID = 0000;

void setup(){
  size(800,600);
  background(0);
}

void draw(){
  if(me == null || !me.active()){
    background(0);
    drawRooms();
  }else if(me.active()){
    
  }
}

void drawRooms(){
  int empty = 0;
  for(int i = 0; i < rooms.length; i++){
    if(rooms[i] != null) rooms[i].create();
    else empty++;
  }
  if(empty == 10){
    textSize(40);
    textAlign(CENTER);
    fill(0,255,0);
    text("No rooms found!",400,300);
  }
}

void clientEvent(Client cli){
  String data = cli.readString();
  String ip = "";
  int dataStart = -1;
  for(int i = 0; i < data.length(); i++){
    if(data.charAt(i) == ':'){
      ip = data.substring(0,i);
      dataStart = i+1;
      break;
    }
  }
  if(ip.equals(me.ip()) && dataStart != -1){
    String dat = data.substring(dataStart,data.length()-1);
    switch(dat.toLowerCase()){
      case "joined":
        player = new Player(joining);
        joined = true;
        break;
      case "failed":
        joining = -1;
        joined = false;
        println("Failed to join room");
        break;
      case "created":
        int created = 0;
        for(int i = 0; i < rooms.length; i++){
          if(rooms[i] != null) created++;
        }
        rooms[created] = new GameRoom(tempID,0,created*(60));
        break;
    }
  }else if(dataStart != -1){
    for(int i = 0; i < rooms.length; i++){
      if(Integer.toString(joining).equals(ip)){
        String dat = data.substring(dataStart,data.length()-1);
        if(!dat.equals(me.ip())){
          if(dat.contains(".")){
            enemy = new Player(joining,dat);
          }
        }
      }
    }
  }
}

void createRoom(int id){
  me.write(me.ip() + ":" + id);
}

void keyPressed(){
  tempID = (int)random(1000,9999);
  createRoom(tempID);
}