class Options{
  Player player;
  boolean enabled = false;
  TextBox size;
  Button disconnect;
  
  Options(Player p){
    player = p;
    size = new TextBox(1280/2,720/2,Float.toString(p.size));
    disconnect = new Button(1280/2,(720/3)*2,200,75,"Kick");
  }
  
  void create(){
    //Major box
    fill(0);
    rect(0,0,1280,720);
    textAlign(LEFT);
    textSize(30);
    fill(255,0,0);
    text("Size: ",size.getX()-100,size.y);
    text("IP: " + player.ip,1280/2,200);
    size.create();
    disconnect.create();
  }
  
  void reset(){
    size.reset();
  }
}

class TextBox{
  private float x,y;
  private String text = "Example text";
  private String startingText = "Example text";
  boolean enabled = false;
  private float width,height;
  
  TextBox(float x, float y){
    this.x = x;
    this.y = y;
  }
  
  TextBox(float x, float y, String text){
    this.x = x;
    this.y = y;
    this.text = text;
    this.startingText = text;
  }
  
  void create(){
    fill(155,155,155);
    rect(x-25,y,(text.length()*25)+50,60);
    fill(0,0,255);
    textSize(50);
    textAlign(CENTER);
    fill(0,0,255);
    text(text,x+(text.length()*25)/2,y+50);
  }
  
  boolean hovered(){
    return(mouseX>x-25 && mouseX<x+(text.length()*25)+50 && mouseY>y && mouseY<y+60);
  }
  
  float getX(){
    return x;
  }
  
  void event(){
    text = "";
    enabled = true;
  }
  
  void deslect(){
    enabled = false;
  }
  
  boolean isEnabled(){
    return enabled;
  }
  
  String getText(){
    return text;
  }
  
  void addLetter(char c){
    this.text += c;
  }
  void removeLetter(){
    if(this.text.length()>0) this.text = this.text.substring(0,text.length()-1);
  }
  
  void reset(){
    enabled = false;
    text = startingText;
    cursor(ARROW);
  }
}

class Button{
  float x,y,width,height;
  String text;
  
  Button(float x, float y, float width, float height, String txt){
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.text = txt;
  }
  
  void create(){
    fill(155,155,155);
    rect(x,y,this.width,this.height);
    textAlign(LEFT);
    textSize(sqrt(width)+sqrt(height));
    fill(255,0,0);
    text(text,x+this.width/3,y+this.height/3);
  }
  
  boolean hovered(){
    return(mouseX>x && mouseX<x+this.width && mouseY>y && mouseY<y+this.height);
  }
}