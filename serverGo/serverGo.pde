
import processing.net.*;
import java.util.*;
ArrayList<Player> players = new ArrayList<Player>();
ArrayList<Client> clients = new ArrayList<Client>();
ArrayList<Options> options = new ArrayList<Options>();
//Server
Server server;
void setup(){
 // println("wow");
  server = new Server(this,5204);
  fullScreen();
  frameRate(30);
  background(0);
  println(server.ip());
  //println("oof");
}

void draw(){
  try{
  background(0);
  Client sending;
  sending = server.available();
  if(sending != null){
    String dat = sending.readString();
    for(int i = 0; i < players.size(); i++){
      if(players.get(i).ip.equals(sending.ip())){
        if(isNumber(dat)){
          int code = Integer.parseInt(dat);
          switch(code){
            case UP:
              players.get(i).ydir = -1;
              players.get(i).xdir = 0;
              break;
            case DOWN:
              players.get(i).ydir = 1;
              players.get(i).xdir = 0;
              break;
            case LEFT:
              players.get(i).ydir = 0;
              players.get(i).xdir = -1;
              break;
            case RIGHT:
              players.get(i).ydir = 0;
              players.get(i).xdir = 1;
              break;
            default:
              println("wrong key!");
              break;
          }
        }else if(dat.contains(",")){
          String data = dat;
         println(data);
          int red = -1;
          int green = -1;
          int blue = -1;
          int endIndex = 0;
          for(int g = 0; g < data.length(); g++){
            if(data.charAt(g) == ','){
              if(red == -1){ 
                red = Integer.parseInt(data.substring(endIndex,g));
                endIndex = g+1;
              }else if(green == -1){ 
                green = Integer.parseInt(data.substring(endIndex,g));
                endIndex = g+1;
                blue = Integer.parseInt(data.substring(endIndex,data.length()));
                endIndex = g+1;
              }
            }
          }
          players.get(i).colors[0] = red;
          players.get(i).colors[1] = green;
          players.get(i).colors[2] = blue;
        }
        sending.clear();
      }
    }
  }
  /*
  for(int i = 0; i < players.size(); i++){
    if(players.get(i).hovered()){
      println("Working");
      textAlign(RIGHT);
      textSize(35);
      fill(255,0,0);
      text(players.get(i).ip,mouseX,mouseY);
    }else println("failed");
  }
  */
  int free = 0;
  for(int i = 0; i < options.size(); i++){
    if(options.get(i).enabled){
      if(options.get(i).size.hovered()) cursor(HAND);
      else if(options.get(i).disconnect.hovered()) cursor(HAND);
      else cursor(ARROW);
    }
    if(options.get(i).enabled){
      options.get(i).create();
    }else free++;
  }
  if(free==options.size()) drawPlayers();
  movePlayers();
  sendData();
  colliding();
  
  
   for(int i = 0; i < players.size(); i++)
   {
     players.get(i).Grow();
   }


  free = 0;
  }
  catch(Exception e){
    for(int i = 0; i < clients.size(); i++){
      server.disconnect(clients.get(i));
    }
     exit();
  }
}

void disconnectEvent(Client cli){
  try{
    for(int i = 0; i < players.size(); i++){
      if(players.get(i).ip.equals(cli.ip())){
        println("removed player");
        players.remove(i);
        break;
      }
      else println(players.get(i).ip);
    }
    for(int i = 0; i < clients.size(); i++){
      if(clients.get(i).ip().equals(cli.ip())){
        clients.remove(i);
        break;
      }
    }
    for(int i = 0; i < options.size(); i++){
      if(options.get(i).player.ip.equals(cli.ip())) options.remove(i);
    }
      sendData();
    println("ran");
  }catch(Exception e){
    for(int i = 0; i < clients.size(); i++){
      server.disconnect(clients.get(i));
    }
    exit();
  }
}

void serverEvent(Server serv,Client cli){
  try{
  players.add(new Player(random(50,1000),random(50,1000),0,cli.ip()));
  clients.add(cli);
  options.add(new Options(players.get(players.size()-1)));
  println("player added");
     sendData();
    println(players.size());
  }catch(Exception e){
    for(int i = 0; i < clients.size(); i++){
      server.disconnect(clients.get(i));
    }
    exit();
  }
}

class Player{
  float x,y,size;
  String ip;
  int ydir = 0;
  int xdir = 1;
  float speed;
  int[] colors = new int[3];
  int time;
  Player(float x, float y, float size, String ip){
    this.x = x;
    this.y = y;
    this.size = size;
    this.ip = ip;
    this.speed = 50.0/size;
  }
  
  String stringify(){
    String newString = x+","+y+","+size+","+ip;
    return newString;
  }
  
  String[] getData(){
    String[] strs = {Float.toString(x),Float.toString(y),Float.toString(size)};
    return strs;
  }
  
   void Grow()
  {
     if(time >= size)
     {
       size++;
       time = 0;
     }
     
     else
     {
       time++;
     }
  }  
  void move(){
    speed = 100.0/size;
    if(x+(speed*xdir)+(size/2)<1280 && x+(speed*xdir)-(size/2)>0) x+=(speed*xdir);
    else if(x+(speed*xdir)+(size/2)<1280) x = 0+(size/2);
    else if(x+(speed*xdir)-(size/2)>0) x = 1280 - (size/2);
    if(y+(speed*ydir)+(size/2)<1024 && y+(speed*ydir)-(size/2)>0)y+=(speed*ydir);
    else if(y+(speed*ydir)+(size/2)<1024) y = 0+(size/2);
    else if(y+(speed*ydir)-(size/2)>0) y = 1024 - (size/2);
  }
  
  boolean hovered(){
    return(mouseX>x-(size/2) && mouseX<x+(size/2) && mouseY>y-(size/2) && mouseY<y+(size/2));
  }
}

void keyReleased(){
  for(int i = 0; i < options.size(); i++){
    if(options.get(i).enabled){
      if(key == '\\'){
        options.get(i).enabled = false;
        options.get(i).reset();
        break;
      }
      if(options.get(i).size.enabled){
        if(keyCode == BACKSPACE) options.get(i).size.removeLetter();
        else if(keyCode == ENTER){
          options.get(i).enabled = false;
          if(isFloat(options.get(i).size.text)) options.get(i).player.size = Float.parseFloat(options.get(i).size.text);
          options.get(i).reset();
        }else if(key != CODED){
          options.get(i).size.addLetter(key);
        }
      }else break;
    }
  }
}

void sendData(){
  try{
    String data = JSONtoString();
    server.write(data);
  }catch(Exception e){
    for(int i = 0; i < clients.size(); i++){
      server.disconnect(clients.get(i));
    }
    exit();
  }
}

void mousePressed(){
  for(int i = 0; i < players.size(); i++){
    if(players.get(i).hovered()){
      for(int g = 0; g < options.size(); g++){
        if(options.get(g).player.ip.equals(players.get(i).ip)){
          options.get(g).enabled = true;
        }
      }
    }
  }
  for(int i = 0; i < options.size(); i++){
    if(options.get(i).size.hovered()){
      options.get(i).size.event();
    }else if(options.get(i).disconnect.hovered()){
      for(int g = 0; g < clients.size(); g++){
        if(clients.get(g).ip().equals(options.get(i).player.ip)){
          options.get(i).enabled = false;
          server.disconnect(clients.get(g));
          cursor(ARROW);
        }
      }
    }
  }
  //if(validJSON(JSONtoString())){
    //println(stringToJSON(JSONtoString()).getJSONObject(players.get(0).ip).getString("x"));
  //}
}

void drawPlayers(){
  for(int i = 0; i < players.size(); i++){
    //println(i);
    //println(players.get(i).colors[0]);
    fill(players.get(i).colors[0],players.get(i).colors[1],players.get(i).colors[2]);
    //fill(255,0,0);
    ellipse(players.get(i).x,players.get(i).y,players.get(i).size,players.get(i).size);
  }
}

void movePlayers(){
  for(int i = 0; i < players.size(); i++){
    players.get(i).move();
  }
}

boolean isNumber(String sending){
  try{
    int code = Integer.parseInt(sending);
    if(code>30) return true;
    else return false;
  }
  catch(Exception e){
    return false;
  }
  finally{
    
  }
}

boolean isFloat(String fl){
  try{
    float f = Float.parseFloat(fl);
    if(f>-1) return true;
    else return false;
  }
  catch(Exception e){
    return false;
  }
}

String JSONtoString(){
  String str = "{";
  for(int i = 0; i < players.size(); i++){
    str+="\"" + players.get(i).ip + "\":{";
    //"ip":{
    for(int g = 0; g < 6; g++){
      switch(g){
        case 0:
          str+="\"x\":\"" + players.get(i).x + "\",";
          //"ip":{"x":"xval",
          break;
        case 1:
          str+="\"y\":\"" + players.get(i).y + "\",";
          //"ip":{"x":"xval","y":"yval",
          break;
        case 2:
          str+="\"size\":\"" + players.get(i).size + "\",";
          //"ip":{"x":"xval","y":"yval","size":"sizeVal"}
          break;
        case 3:
          str+="\"red\":\"" + players.get(i).colors[0] + "\",";
          break;
          
        case 4:
          str+="\"green\":\"" + players.get(i).colors[1] + "\",";
          break;
          
        case 5:
          str+="\"blue\":\"" + players.get(i).colors[2] + "\"}";
          break;
      }
    }
    if(i+1<players.size()) str+=",";
  }
  str+="}";
  return str;
}

boolean validJSON(String jsonString){
  try{
    JSONObject obj = parseJSONObject(jsonString);
    return true;
  }
  catch(Exception e){
    println("failed");
    println(jsonString);
    return false;
  }
}

JSONObject stringToJSON(String s){
  JSONObject test = parseJSONObject(s);
  Set keys = test.keys();
  Iterator<?> keyIt = keys.iterator();
  while(keyIt.hasNext()){
    println(keyIt.next());
  }
  return parseJSONObject(s);
}

void colliding(){
  if(players.size() >=2 )
  {
        float size;
  
   for(int checkOne = 0; checkOne < players.size(); checkOne++){
     for(int checkTwo = 0; checkTwo < players.size(); checkTwo++){
     if(players.get(checkOne).ip != players.get(checkTwo).ip){
     
     if(players.get(checkOne).size > players.get(checkTwo).size){size = players.get(checkOne).size;}
     else{size = players.get(checkTwo).size;}
     
        if(dist(players.get(checkOne).x, players.get(checkOne).y,players.get(checkTwo).x,players.get(checkTwo).y) < size/2){
      
            if(players.get(checkOne).size > players.get(checkTwo).size){
              players.get(checkOne).size += players.get(checkTwo).size/3;
              server.disconnect(clients.get(checkTwo));
            }
        
            else if (players.get(checkTwo).size > players.get(checkOne).size) {
              players.get(checkTwo).size += players.get(checkOne).size/3;
              server.disconnect(clients.get(checkOne));
            }
        }
       }
     }
   }
  }
}