class TextBox{
  private float x,y;
  private String text = "Example text";
  private String startingText = "Example text";
  boolean enabled = false;
  private float width,height;
  
  TextBox(float x, float y){
    this.x = x;
    this.y = y;
  }
  
  TextBox(float x, float y, String text){
    this.x = x;
    this.y = y;
    this.text = text;
    this.startingText = text;
  }
  
  void create(){
    fill(155,155,155);
    rect(x-25,y,(text.length()*25)+50,60);
    fill(0,0,255);
    textSize(50);
    textAlign(CENTER);
    text(text,x+(text.length()*25)/2,y+50);
  }
  
  boolean hovered(){
    return(mouseX>x-25 && mouseX<x+(text.length()*25)+50 && mouseY>y && mouseY<y+60);
  }
  
  float getX(){
    return x;
  }
  
  void event(){
    text = "";
    enabled = true;
  }
  
  void deslect(){
    enabled = false;
  }
  
  boolean isEnabled(){
    return enabled;
  }
  
  String getText(){
    return text;
  }
  
  void addLetter(char c){
    this.text += c;
  }
  void removeLetter(){
    if(this.text.length()>0) this.text = this.text.substring(0,text.length()-1);
  }
  
  void reset(){
    enabled = false;
    text = startingText;
  }
}

class Leaderboard{
  int[] sizes;
  
  Leaderboard(){
    sizes = new int[players.size()];
  }
  
  void update(){
    int[] sizes2 = new int[players.size()];
    for(int i = 0; i < players.size(); i++){
      sizes2[i] = (int)players.get(i).size;
    }
    Arrays.sort(sizes2);
    sizes = new int[players.size()];
    int ind = sizes.length-1;
    for(int i = 0; i < sizes2.length; i++){
      sizes[i] = sizes2[ind];
      ind--;
    }
  }
  
  void create(){
    for(int i = 0; i < 10; i++){
      if(noErrors(i)){
        fill(155,155,155);
        rect(1180,50*i,100,50);
        Player p = players.get(0);
        for(int g = 0; g < players.size(); g++){
          if(players.get(g).size == sizes[i]){ p = players.get(g); break;}
        }
        fill(p.red,p.green,p.blue);
        textSize(20);
        text(i+1+":",1180-15,(i*50)+38);
        text(sizes[i],1180+35,(50*i)+38);
      }else break;
    }
  }
  
  boolean noErrors(int i){
    try{
      int test = sizes[i];
      if(test>-1) return true;
      else return false;
    }
    catch(Exception e){
      return false;
    }
  }
}