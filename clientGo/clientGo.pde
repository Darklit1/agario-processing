import processing.net.*;
import java.util.*;
ArrayList<Player> players = new ArrayList<Player>();
//Client
Client me;
TextBox txt = new TextBox(1280/2,720/2,"10.26.129.192");

Leaderboard leaders;
TextBox r = new TextBox(1280/4,(720/3)*2,"255");
TextBox g = new TextBox((1280/4)*2,(720/3)*2,"0");
TextBox b = new TextBox((1280/3)*2,(720/3)*2,"0");
boolean connected = false;
void setup(){
  fullScreen();
  println(height);
  background(0);
  textSize(60);
  fill(255,0,0);
  text("Connecting...",1280/2,360);
  delay(1000);
  /*me = new Client(this,"192.168.1.221",5204);
  if(me.active()){
    players.add(new Player(random(200,400),200,20,me.ip(),Integer.parseInt(r.getText()),Integer.parseInt(g.getText()),Integer.parseInt(b.getText())));
    me.write(r.getText()+","+b.getText()+","+g.getText());
    connected = true;
  }
  */
}

void draw(){
  background(0);
  if(me != null){
    if(me.active()){
      if(me.available()>0){
        String data = me.readString();
        readData(data);
        me.clear();
      }
      removeDuplicates();
      drawPlayers();
      r.reset();
      g.reset();
      b.reset();
      txt.reset();
      leaders.update();
      leaders.create();
    }else{
      connected = false;
      leaders = null;
      players = new ArrayList<Player>();
      txt.create();
      textAlign(CENTER);
      text("Ip: ",txt.getX()-40,(720/2));
      r.create();
      text("Red",r.getX(),(720/3)*2);
      b.create();
      text("Blue",b.getX(),(720/3)*2);
      g.create();
      text("Green",g.getX(),(720/3)*2);
      if(txt.hovered() || r.hovered() || g.hovered() || b.hovered()) cursor(HAND);
      else cursor(ARROW);
    }
  }
  else{
    connected = false;
    players = new ArrayList<Player>();
    txt.create();
    textAlign(CENTER);
    text("Ip: ",txt.getX()-40,(720/2));
    r.create();
    text("Red",r.getX(),(720/3)*2);
    b.create();
    text("Blue",b.getX(),(720/3)*2);
    g.create();
    text("Green",g.getX(),(720/3)*2);
    if(txt.hovered() || r.hovered() || g.hovered() || b.hovered()) cursor(HAND);
    else cursor(ARROW);
  }
}

class Player{
  private float x;
  private float y;
  private float size;
  private String ip;
  boolean justAdded = false;
  int red;
  int green;
  int blue;

  Player(float x, float y, float size, String ip, int r, int g, int b){
    this.x = x;
    this.y = y;
    this.size = size;
    this.ip = ip;
    red = r;
    green = g;
    blue = b;
  }
  void create(){
    ellipse(x,y,size,size);
  }

  void setX(float xpos){
    x = xpos;
  }

  void setY(float ypos){
    y = ypos;
  }

  void setSize(float si){
    size = si;
  }

  float getX(){
    return x;
  }

  String getIP(){
    return ip;
  }
  
  void setColors(int r, int g, int b){
    red = r;
    green = g;
    blue = b;
  }


}


void readData(String data){
  if(validJSON(data)){
    JSONObject dataObj = stringToJSON(data);
    Set keys = dataObj.keys();
    Iterator<?> keyIt = keys.iterator();
    ArrayList<Player> safePlayers = new ArrayList<Player>();
    while(keyIt.hasNext()){
      String keyy = (String)keyIt.next();
      println(keyy);
      int t = 0;
      for(int i = 0; i < players.size(); i++){
        if(keyy.equals(players.get(i).getIP())){
          //Set
          println("set");
          JSONObject playerObj = dataObj.getJSONObject(players.get(i).getIP());
          players.get(i).setX(Float.parseFloat(playerObj.getString("x")));
          players.get(i).setY(Float.parseFloat(playerObj.getString("y")));
          players.get(i).setSize(Float.parseFloat(playerObj.getString("size")));
          players.get(i).setColors(Integer.parseInt(playerObj.getString("red")),Integer.parseInt(playerObj.getString("green")),Integer.parseInt(playerObj.getString("blue")));
          break;
        }else t++;
      }
      if(t == players.size()){
        println("added");
        if(keyy.contains(".")){
          JSONObject playerObj = dataObj.getJSONObject(keyy);
          players.add(new Player(Float.parseFloat(playerObj.getString("x")),Float.parseFloat(playerObj.getString("y")),Float.parseFloat(playerObj.getString("size")),keyy,Integer.parseInt(playerObj.getString("red")),Integer.parseInt(playerObj.getString("green")),Integer.parseInt(playerObj.getString("blue"))));
          players.get(players.size()-1).justAdded = true;
        }
      }

      for(int i = 0; i < players.size(); i++){
          if(players.get(i).getIP().equals(keyy) || players.get(i).justAdded){
            safePlayers.add(players.get(i));
            players.get(i).justAdded = false;
            break;
          }else{
            println(players.get(i).getIP());
            println("----");
            println(keyy);
          }
        }

    }
    println(safePlayers.size());
    players = new ArrayList<Player>();
    for(int i = 0; i < safePlayers.size(); i++){
      players.add(safePlayers.get(i));
    }
  }
}


void removeDuplicates(){
  for(int i = 0; i < players.size(); i++){
    for(int g = i+1; g < players.size(); g++){
      if(players.get(i).getIP().equals(players.get(g).getIP())){
        println("dupe player removed");
        players.remove(g);
      }
    }
  }
}

void exit(){
  if(me != null) me.stop();
  System.exit(0);
}

boolean validJSON(String jsonString){
  try{
    JSONObject obj = parseJSONObject(jsonString);
    return true;
  }
  catch(Exception e){
    println("failed");
    println(jsonString);
    return false;
  }
}

JSONObject stringToJSON(String s){
  return parseJSONObject(s);
}

boolean checkErrors(String[] dat){
  try{
    float x = Float.parseFloat(dat[0]);
    float y = Float.parseFloat(dat[1]);
    float size = Float.parseFloat(dat[2]);
    String ip = dat[3];
  }
  catch(Exception e){
    println(e);
    return true;
  }
  finally{
    return false;
  }
}

Player[] toArray(){
  Player[] playerz = new Player[players.size()];
  for(int i = 0; i < players.size(); i++){
    playerz[i] = players.get(i);
  }
  return playerz;
}

void keyReleased(){
  if(connected) me.write
  (Integer.toString(keyCode));
  else if(txt.isEnabled()){
    if(keyCode == BACKSPACE) txt.removeLetter();
    else if(keyCode == ENTER){
      me = new Client(this,txt.getText(),5204);
      leaders = new Leaderboard();
      connected = true;
      if(me.active()){
        txt.startingText = txt.getText();
        int red = Integer.parseInt(r.getText());
        int green = Integer.parseInt(g.getText());
        int blue = Integer.parseInt(b.getText());
        players.add(new Player(200,200,10,me.ip(),red,green,blue));
        me.write(red+","+green+","+blue);
      }
      noCursor();
      txt.reset();
      r.reset();
      g.reset();
      b.reset();
    }
    else if(key != CODED){
      txt.addLetter(key);
    }
  }else if(r.isEnabled() || b.isEnabled() || g.isEnabled()){
    if(keyCode == BACKSPACE){
      if(r.isEnabled()) r.removeLetter();
      else if(b.isEnabled()) b.removeLetter();
      else if(g.isEnabled()) g.removeLetter();
    }
    else if(keyCode == ENTER){
      me = new Client(this,txt.getText(),5204);
      connected = true;
      leaders = new Leaderboard();
      if(me.active()){
        txt.startingText = txt.getText();
        println("connected");
        int red = 255;
        int green = 0;
        int blue = 0;
        try{
          red = Integer.parseInt(r.getText());
          green = Integer.parseInt(g.getText());
          blue = Integer.parseInt(b.getText());
        }
        catch(Exception e){
          println("Wrong color values!");
        }
        if(red>255) red = 255;
        if(green>255) green = 255;
        if(blue>255) blue = 255;
        players.add(new Player(200,200,10,me.ip(),red,green,blue));
        me.write(red+","+green+","+blue);
      }
      noCursor();
      txt.reset();
      r.reset();
      g.reset();
      b.reset();
    }
    else if(key != CODED){
      if(r.isEnabled()) r.addLetter(key);
      else if(g.isEnabled()) g.addLetter(key);
      else if(b.isEnabled()) b.addLetter(key);
    }
  }else if(keyCode == ENTER && !connected){
      me = new Client(this,txt.getText(),5204);
      leaders = new Leaderboard();
      connected = true;
      if(me.active()){
        txt.startingText = txt.getText();
        int red = Integer.parseInt(r.getText());
        int green = Integer.parseInt(g.getText());
        int blue = Integer.parseInt(b.getText());
        players.add(new Player(200,200,10,me.ip(),red,green,blue));
        me.write(red+","+green+","+blue);
      }
    }
  //println(keyCode);
}

void drawPlayers(){
  int[] sizes = new int[players.size()];
  ArrayList<Player> drawPlay = new ArrayList<Player>(players.size());
  for(int i = 0; i < players.size(); i++){
    sizes[i] = (int)players.get(i).size;
    drawPlay.add(players.get(i));
  }
  Arrays.sort(sizes);
  for(int i = 0; i < sizes.length; i++){
    for(int g = 0; g < drawPlay.size(); g++){
      if(sizes[i] == (int)drawPlay.get(g).size){
        fill(drawPlay.get(g).red,drawPlay.get(g).green,drawPlay.get(g).blue);
        ellipse(drawPlay.get(g).x,drawPlay.get(g).y,drawPlay.get(g).size,drawPlay.get(g).size);
        drawPlay.remove(g);
        break;
      }
    }
  }
}

void mousePressed(){
  if(connected){
    int red = (int)random(255);
    int green = (int)random(255);
    int blue = (int)random(255);
    me.write(red+","+green+","+blue);
  }else if(txt.hovered()) txt.event();
  else if(r.hovered()){ r.event(); g.enabled = false; b.enabled = false; txt.enabled = false;}
  else if(g.hovered()){ g.event(); r.enabled = false; b.enabled = false; txt.enabled = false;}
  else if(b.hovered()){ b.event(); r.enabled = false; g.enabled = false; txt.enabled = false;}
  println(players.size());
}